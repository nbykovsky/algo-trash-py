
def edit_distance(str1, str2):
    dp = [[10**19]*(len(str1)) for _ in range(len(str2))]
    for i in range(len(str1)):
        dp[0][i] = i
    for j in range(len(str2)):
        dp[j][0] = j

    for i in range(len(str1)):
        for j in range(len(str2)):
            if i == j == 0:
                if str1[i] == str2[j]:
                    dp[j][i] = 0
                else:
                    dp[j][i] = 1
            elif j == 0:
                dp[j][i] = min(i + 1, dp[j][i-1] + 1)
            elif i == 0:
                dp[j][i] = min(j + 1, dp[j-1][i] + 1)
            elif str1[i] == str2[j]:
                dp[j][i] = min(dp[j - 1][i - 1], dp[j][i - 1] + 1, dp[j - 1][i] + 1)
            else:
                dp[j][i] = min(dp[j - 1][i - 1] + 1, dp[j][i - 1] + 1, dp[j - 1][i] + 1)

    return dp[-1][-1]


if __name__ == '__main__':
    print(edit_distance('abc', 'abc'))
    print(edit_distance('abc', 'abd'))
    print(edit_distance('abc', 'aed'))
    print(edit_distance('abc', 'wed'))

