import math


def get_primes(n):
    result = set()
    while n % 2 == 0:
        result.add(2)
        n = n / 2

    for i in range(3, int(math.sqrt(n)) + 1, 2):

        while n % i == 0:
            result.add(i)
            n = n / i

    if n > 2:
        result.add(n)
    return list(result)