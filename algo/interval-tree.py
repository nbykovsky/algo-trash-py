from typing import List


class NumArray:

    def __init__(self, nums: List[int]):
        n = len(nums) - 1
        p = 0
        while n > 0:
            n >>= 1
            p += 1
        size = 2 ** (p+1) - 1
        self._start = 2 ** p - 1
        self._arr = [0] * size
        for i in range(len(nums)):
            self._arr[self._start + i] = nums[i]

        for i in range(len(self._arr)-1, 0, -1):
            self._arr[(i-1)//2] += self._arr[i]

    def update(self, i: int, val: int) -> None:
        j = self._start + i
        prev = self._arr[j]
        self._arr[j] = val
        while j != 0:
            j -= 1
            j //= 2
            self._arr[j] -= prev
            self._arr[j] += val

    def sumRange(self, i: int, j: int) -> int:
        acc = 0
        l = i + self._start
        r = j + self._start
        while l <= r:
            if l % 2 == 0:
                acc += self._arr[l]
                l = (l - 1) // 2 + 1
            else:
                l = (l - 1) // 2

            if r % 2 == 1:
                acc += self._arr[r]
                r = (r - 1) // 2 - 1
            else:
                r = (r - 1) // 2

        return acc



if __name__ == '__main__':
    n = 8
    x = NumArray(list(range(n)))
    # x.update(0, 5)
    for i in range(n):
        for j in range(i, n):
            print(sum(list(range(i, j + 1))), x.sumRange(i, j))