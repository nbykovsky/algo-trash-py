# problem https://practice.geeksforgeeks.org/problems/game-of-death-in-a-circle/0


def josephus(n, k):
    """
    Solution of josephus problem for arbitrary shift (k)

    https://www.geeksforgeeks.org/josephus-problem-set-1-a-on-solution/
    """
    if n == 1:
        return 1
    else:
        return (josephus(n - 1, k) + k - 1) % n + 1


def msb_position(n):
    pos = 0
    while n >> pos != 0:
        pos += 1
    return pos


def josephus2(n):
    """
    Solution of josephus problem for k = 2

    https://www.geeksforgeeks.org/josephus-problem-using-bit-magic/
    """
    msb = msb_position(n)
    msb_mask = 1 << (msb - 1)

    # getting rid of MSB
    shift = n ^ msb_mask
    # multiply by 2
    shift = shift << 1
    # adding one
    shift = shift | 1
    return shift


if __name__ == '__main__':
    for _ in range(int(input())):
        n, k = map(int, input().split())
        print(josephus(n, k))

