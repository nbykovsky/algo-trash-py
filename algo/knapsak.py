
def with_repetitions(values, w):
    dp = [0] * (w + 1)
    for i in range(1, w + 1):
        for wi, vi in values:
            if wi <= i:
                dp[i] = max(dp[i], dp[i - wi] + vi)

    return dp[-1]


def without_repetitions(values, w):
    dp = [[0] * (w + 1) for _ in range(len(values) + 1)]
    for v in range(1, len(values) + 1):
        for i in range(1, w + 1):
            if values[v - 1][0] <= i:
                dp[v][i] = max((dp[v][i], dp[v - 1][i - values[v - 1][0]] + values[v - 1][1], dp[v - 1][i]))

    return dp[-1][-1]


if __name__ == '__main__':
    arr = [(6, 30), (3, 14), (4, 16), (2, 9)]
    print(with_repetitions(arr, 10))
    print(without_repetitions(arr, 10))
