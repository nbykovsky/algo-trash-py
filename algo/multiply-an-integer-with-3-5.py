# problem https://www.geeksforgeeks.org/multiply-an-integer-with-3-5/


def mult(n):
    return (n + (n << 1) + (n << 2)) >> 1


if __name__ == '__main__':
    print(mult(2))
    print(mult(5))
