"""
Multiplying two polynomials using Karatsuba algorithm

---------------------------------------

Enter length of polynomials:
5000 5000
Multiplying two polynomials 5000 and 5000 values
Naive execution time: 88.54
Karatsuba execution time: 58.59
---------------------------------------
"""
import random
from operator import add
import time


def _adjustment(a, b):
    """
    Align arrays and make length even
    """
    power = max(len(a), len(b))
    power += power % 2
    return ([0] * (power - len(a))) + a, ([0] * (power - len(b))) + b, power


def naive(a_init, b_init):
    """
    Naive multiplication O(n^2)
    """
    if len(a_init) == len(b_init) == 1:
        return [a_init[0] * b_init[0]]

    a, b, n = _adjustment(a_init, b_init)
    result = [0] * n * 2
    # n is even by definition of _adjustment
    h = n//2
    result[:n] = naive(a[:h], b[:h])
    result[n:] = naive(a[h:], b[h:])
    middle = map(add, naive(a[:h], b[h:]), naive(a[h:], b[:h]))
    for i, m in enumerate(middle):
        result[h+i] += m
    return result[1 - len(a_init)-len(b_init):]


def karatsuba(a_init, b_init):
    """
    Karatsuba algorithm O(n^1.58)
    """
    if len(a_init) == len(b_init) == 1:
        return [a_init[0] * b_init[0]]

    a, b, n = _adjustment(a_init, b_init)
    result = [0] * n * 2
    # n is even by definition of _adjustment
    h = n//2
    result[:n] = naive(a[:h], b[:h])
    result[n:] = naive(a[h:], b[h:])
    middle = naive(list(map(add, a[:h], a[h:])), list(map(add, b[:h], b[h:])))
    for i, m in enumerate(middle):
        result[h+i] += m - result[i] - result[n + i]
    return result[1-len(a_init)-len(b_init):]


if __name__ == '__main__':
    n1, n2 = map(int, input("Enter length of polynomials divided by space: \n").strip().split())

    # generation random polynomials of given size
    arr1, arr2 = [random.randint(1, 10000) for _ in range(n1)], [random.randint(1, 10000) for _ in range(n2)]
    print("Multiplying two polynomials %s and %s values" % (len(arr1), len(arr2)))

    start_naive = time.time()
    naive(arr1, arr2)
    end_naive = time.time()
    print("Naive execution time: %s " % round(end_naive - start_naive, 2))

    start_karatsuba = time.time()
    karatsuba(arr1, arr2)
    end_karatsuba = time.time()
    print("Karatsuba execution time: %s " % round(end_karatsuba - start_karatsuba, 2))


