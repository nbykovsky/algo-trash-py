import random


def selection_sort(arr_init):
    arr = arr_init[:]
    for i in range(0, len(arr)):
        for j in range(i, len(arr)):
            if arr[j] < arr[i]:
                arr[i], arr[j] = arr[j], arr[i]
    return arr


def merge_sort(arr_init):
    if len(arr_init) < 2:
        return arr_init

    mid = int(len(arr_init)/2)
    arr1 = list(reversed(merge_sort(arr_init[:mid])))
    arr2 = list(reversed(merge_sort(arr_init[mid:])))

    result = []
    while len(arr1) > 0 and len(arr2) > 0:
        if arr1[-1] > arr2[-1]:
            result.append(arr2[-1])
            arr2.pop()
        else:
            result.append(arr1[-1])
            arr1.pop()

    if not arr1:
        result.extend(reversed(arr2))
    elif not arr2:
        result.extend(reversed(arr1))

    return result


def count_sort(arr, n):
    counts = [0] * n
    for a in arr:
        counts[a] += 1

    pos = [-1] * n
    pos[0] = 0
    for i in range(1, n):
        pos[i] = pos[i - 1] + counts[i - 1]

    result = [-1] * len(arr)

    for a in arr:
        result[pos[a]] = a
        pos[a] += 1

    return result


def partition(arr, l, r):
    p = random.randint(l, r - 1)
    arr[l], arr[p] = arr[p], arr[l]
    m = l
    e = l + 1
    while e != r:
        if arr[e] <= arr[l]:
            arr[e], arr[m + 1] = arr[m + 1], arr[e]
            m += 1
        e += 1
    arr[l], arr[m] = arr[m], arr[l]

    return m


def quick_sort(arr, l, r):
    if l + 1 >= r:
        return arr
    m = partition(arr, l, r)

    quick_sort(arr, l, m)
    quick_sort(arr, m + 1, r)
    return arr


if __name__ == '__main__':
    a = [100, 24, 23, 56, 13, 1, 4, 4, 4, 0, -1]
    print(selection_sort(a))
    print(merge_sort(a))
    print(count_sort([1,5,4,4,3,2,1,2,3,4,4,3,2,1,0], 6))
    print(quick_sort(a[:], 0, len(a)))
