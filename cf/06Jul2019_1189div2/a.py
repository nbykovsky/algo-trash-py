from collections import Counter

if __name__ == '__main__':
        n = int(input())
        ss = list(input())
        cnt = Counter(ss)
        if '1' in cnt and '0' in cnt and cnt['1'] == cnt['0']:
            print(2)
            print("".join(ss[:1]),"".join(ss[1:]))
        else:
            print(1)
            print("".join(ss))