
if __name__ == '__main__':
    n = int(input())
    aa = list(map(int, input().split()))
    aa.sort(reverse=True)
    acc1 = []
    acc2 = []
    for i in range(1, n, 2):
        acc1.append(aa[i])

    for i in range(2, n, 2):
        acc2.append(aa[i])
    if acc1[0] + acc2[0] <= aa[0]:
        print("NO")
    else:
        print("YES")
        res = list(reversed(acc1))
        res.append(aa[0])
        res.extend(acc2)
        print(" ".join(map(str, res)))