

if __name__ == '__main__':
    n = int(input())
    ss = list(input())

    m = 10 ** 19
    for i in range(0, n - 3):
        def f(c, j):
            return min((26 + ord(c) - ord(ss[i + j])) % 26, (26 - ord(c) + ord(ss[i + j])) % 26 )

        x = 0
        x += f('A', 0)
        x += f('C', 1)
        x += f('T', 2)
        x += f('G', 3)
        m = min(m, x)
    print(m)