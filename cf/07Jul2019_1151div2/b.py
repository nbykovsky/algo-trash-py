from collections import Counter

if __name__ == '__main__':
    n, m = map(int, input().split())
    arr = []
    for _ in range(n):
        arr.append(list(map(int, input().split())))

    res = ['1'] * n
    acc = 0
    for i in range(n):
        acc ^= arr[i][0]
    if acc > 0:
        print("TAK")
        print(" ".join(res))
    else:
        i = 0
        while i < n and len(Counter(arr[i])) == 1:
            i += 1
        if i == n:
            print("NIE")
        else:
            j = 0
            while arr[i][j] == arr[i][0]:
                j += 1
            res[i] = str(j + 1)
            print("TAK")
            print(" ".join(res))
