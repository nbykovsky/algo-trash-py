from sys import  stdin

if __name__ == '__main__':
    n = int(input())
    ab = []
    lines = stdin.readlines()
    for line in lines:
        a, b = map(int, line.split())
        ab.append((a, b))

    ab.sort(key=lambda x: max(x[0], x[1]) - min(x[0], x[1]), reverse=True)
    l = 0
    r = n - 1
    ans = 0
    for a, b in ab:
        if a > b:
            ans += a * l + b * (n - l - 1)
            l += 1
        else:
            ans += a * r + b * (n - r - 1)
            r -= 1
    print(ans)