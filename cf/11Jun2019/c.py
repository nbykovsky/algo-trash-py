import sys


def key(s):
    a = list(filter(lambda x: x in ('a', 'e', 'o', 'i', 'u'), s))
    return len(a), a[-1]


if __name__ == '__main__':
    n = int(input())
    ss = map(lambda x: x.strip(), sys.stdin.readlines())
    srt = sorted(ss, key=key)
    pairs = []
    tmp = []
    i = 0
    while i < n - 1:
        k1 = key(srt[i])
        k2 = key(srt[i+1])
        if k1 == k2:
            pairs.append((srt[i], srt[i+1]))
            i += 2
        else:
            tmp.append(srt[i])
            i += 1

    while i < n:
        tmp.append(srt[i])
        i += 1

    rest = []
    i = 0
    while i < len(tmp) - 1:
        k1 = key(tmp[i])
        k2 = key(tmp[i + 1])
        if k1[0] == k2[0]:
            rest.append((tmp[i], tmp[i + 1]))
            i += 2
        else:
            i += 1

    if len(pairs) <= len(rest):
        result = list(zip(rest, pairs))
    else:
        off = (len(pairs) - len(rest)) // 2
        result = list(zip(rest + pairs[(len(pairs) - off):], pairs[:(len(pairs) - off)]))

    print(len(result))
    output = []
    for l, r in result:
        output.append("%s %s" % (l[0], r[0]))
        output.append("%s %s" % (l[1], r[1]))

    print("\n".join(output))

