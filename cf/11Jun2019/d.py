from collections import deque, Counter
import sys


def check_node(g, node, n):
    vis = [False] * n
    q = deque()
    vis[node] = True
    q.append((node, 0))
    l_p = -1
    adj = -1
    while len(q) > 0:
        v, l = q.popleft()

        if l_p == l:
            if len(g[v]) != adj:
                return False
        else:
            adj = len(g[v])
            l_p = l

        for u in g[v]:
            if not vis[u]:
                vis[u] = True
                q.append((u, l + 1))
    return True


def length(g, node, n):
    vis = [False] * n
    s = deque()
    vis[node] = True
    s.append((-1, [node]))
    d = 0
    e = None
    m = None
    while len(s) > 0:
        ls = len(s) - 1
        if ls > d:
            if ls % 2 == 1:
                m = s[ls//2 + 1][0]
                e = s[-1][0]
            else:
                e = s[-1][0]
                m = None
            d = ls

        if not s[-1][1]:
            s.pop()
            continue
        v = s[-1][1].pop()
        s.append((v, [x for x in g[v] if not vis[x]]))
        for x in g[v]:
            vis[x] = True

    return e, m


def get(g, nodes, n, prev):
    vis = [False] * n
    s = deque()
    for node in nodes:
        vis[node] = True
        s.append((node, 0))
    vis[prev] = True

    res = []
    while len(s) > 0:
        v, l = s.pop()
        if len(g[v]) > 2:
            continue
        elif len(g[v]) == 1:
            res.append((v, l))
        for u in g[v]:
            if not vis[u]:
                vis[u] = True
                s.append((u, l + 1))

    return res



if __name__ == '__main__':
    n = int(input())
    if n == 1:
        print(1)
        exit(0)
    g = [[] for _ in range(n)]

    lines = sys.stdin.readlines()

    for line in lines:
        v, u = map(int, line.split())
        g[v - 1].append(u - 1)
        g[u - 1].append(v - 1)

    s = -1
    for i in range(n):
        if len(g[i]) == 1:
            s = i
            break

    if check_node(g, s, n):
        print(s + 1)
        exit(0)

    e, m = length(g, s, n)
    if check_node(g, e, n):
        print(e + 1)
        exit(0)

    if not m:
        print(-1)
        exit(0)

    if check_node(g, m, n):
        print(m + 1)
        exit(0)

    gt = get(g, g[m], n, m)
    if not gt:
        print(-1)
        exit(0)

    ctr = Counter((map(lambda x: x[1], gt)))

    if len(ctr) == 1:
        if check_node(g, gt[0][0], n):
            print(gt[0][0] + 1)
            exit(0)

    if len(ctr) == 2 and 1 in ctr.values():
        p = list(filter(lambda x: x[1] == 1, ctr.items()))[0][0]
        for y in gt:
            if y[1] == p:
                if check_node(g, y[0], n):
                    print(y[0] + 1)
                    exit(0)


    print(-1)

