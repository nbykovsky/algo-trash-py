
if __name__ == '__main__':
    q = int(input())
    for _ in range(q):
        n = int(input())
        cnt = 0
        bad = False
        while n != 1:
            cnt += 1
            if n % 2 == 0:
                n = n // 2
            elif n % 3 == 0:
                n = (n // 3) * 2
            elif n % 5 == 0:
                n = (n // 5) * 4
            else:
                bad = True
                break

        if bad:
            print(-1)
        else:
            print(cnt)