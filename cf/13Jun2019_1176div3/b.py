from collections import Counter

if __name__ == '__main__':
    t = int(input())
    for _ in range(t):
        n = int(input())
        aa = map(lambda x: int(x) % 3, input().split())
        cnt = Counter(aa)
        res = cnt[0]
        if cnt[2] <= cnt[1]:
            res += (cnt[2] + (cnt[1] - cnt[2]) // 3)
        else:
            res += (cnt[1] + (cnt[2] - cnt[1]) // 3)

        print(res)
