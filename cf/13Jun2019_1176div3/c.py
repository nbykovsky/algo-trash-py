from collections import Counter

if __name__ == '__main__':
    n = int(input())
    aa = map(int, input().split())
    cnt = Counter()
    l = 0
    for a in aa:
        if a == 4:
            cnt.update({4:1})
        elif a == 8:
            if cnt[4] > 0:
                cnt.subtract({4:1})
                cnt.update({8:1})
            else:
                l += 1
        elif a == 15:
            if cnt[8] > 0:
                cnt.subtract({8: 1})
                cnt.update({15: 1})
            else:
                l += 1
        elif a == 16:
            if cnt[15] > 0:
                cnt.subtract({15: 1})
                cnt.update({16: 1})
            else:
                l += 1
        elif a == 23:
            if cnt[16] > 0:
                cnt.subtract({16: 1})
                cnt.update({23: 1})
            else:
                l += 1
        elif a == 42:
            if cnt[23] > 0:
                cnt.subtract({23: 1})
                cnt.update({42: 1})
            else:
                l += 1

    d = dict(cnt)
    d[42] = 0
    l += cnt[4]
    l += cnt[8] * 2
    l += cnt[15] * 3
    l += cnt[16] * 4
    l += cnt[23] * 5
    print(l)