
if __name__ == '__main__':
    s = list(map(lambda x: (int(x[0]), x[1]), input().split()))
    st = list(set([x for _, x in s]))
    best = 2
    for a in st:
        mp = sorted(map(lambda t: t[0], list(filter(lambda x: x[1] == a, s))))
        if len(mp) == 3 and (mp[0] == mp[1] == mp[2] or (mp[0] + 1 == mp[1] and mp[1] + 1 == mp[2])):
            best = min(best, 0)
        if (len(mp) >= 2 and (mp[0] + 1 == mp[1] or mp[0] + 2 == mp[1] or mp[0] == mp[1])) or \
                (len(mp) == 3 and (mp[1] + 1 == mp[2] or mp[1] + 2 == mp[2] or mp[1] == mp[2])):
            best = min(best, 1)

    print(best)