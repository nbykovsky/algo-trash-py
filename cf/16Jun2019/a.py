
if __name__ == '__main__':
    x, y, z = map(int, input().split())
    print((x + y) // z, end=" ")
    a = x % z
    b = y % z
    if a + b >= z:
        print(min(a, b, z - a, z - b))
    else:
        print(0)
