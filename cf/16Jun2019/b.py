
if __name__ == '__main__':
    m = int(input())
    n = input()
    l = m // 2
    while l > 0 and n[l] == '0':
        l -= 1

    r = m // 2 + 1
    while r < m and n[r] == '0':
        r += 1

    a = None
    if r < m:
        a = int(n[:r]) + int(n[r:])

    b = None
    if l > 0:
        b = int(n[:l]) + int(n[l:])

    if a and b:
        print(min(a, b))
    else:
        print(a or b)