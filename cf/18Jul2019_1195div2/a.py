from math import ceil


if __name__ == '__main__':
    n, k = map(int, input().split())
    arr = [0] * k
    for _ in range(n):
        a = int(input())
        arr[a - 1] += 1

    i = 0
    c = 2
    for j in range(ceil(n / 2)):

        while i < len(arr) and arr[i] < c:
            i += 1

        if i < len(arr):
            arr[i] -= c
        elif c == 2:
            i = 0
            c = 1
            while i < len(arr) and arr[i] < c:
                i += 1
            if i < len(arr):
                arr[i] -= c

        else:
            break

    print(n - sum(arr))