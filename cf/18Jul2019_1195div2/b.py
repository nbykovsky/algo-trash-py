

def func(n, m):
    return ((1 + (n - m))*(n - m)) // 2 - m


if __name__ == '__main__':
    n, k = map(int, input().split())
    l = 0
    r = n
    while l <= r:
        m = (l + r) // 2
        f = func(n, m)
        if f < k:
            r = m
        elif f > k:
            l = m
        else:
            print(m)
            exit(0)
