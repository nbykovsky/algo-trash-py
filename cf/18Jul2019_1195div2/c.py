
if __name__ == '__main__':
    n = int(input())
    h1 = list(map(int, input().split()))
    h2 = list(map(int, input().split()))
    if n == 1:
        print(max(h1[0], h2[0]))
        exit(0)

    x1 = [0] * n
    x2 = [0] * n
    x1[0] = h1[0]
    x2[0] = h2[0]
    x1[1] = x2[0] + h1[1]
    x2[1] = x1[0] + h2[1]
    for i in range(2, n):
        if i % 2:
            x1[i] = h1[i] + max(x2[i - 1], x2[i - 2])
            x2[i] = h2[i] + max(x1[i - 1], x1[i - 2])
        else:
            x1[i] = h1[i] + max(x2[i - 1], x2[i - 2])
            x2[i] = h2[i] + max(x1[i - 1], x1[i - 2])

    print(max(x1[-1], x2[-1]))

"""
2
1 1
2 2
"""