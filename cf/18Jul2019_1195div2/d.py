
b = 998244353

if __name__ == '__main__':
    n = int(input())
    arr = input().split()
    s = 0
    for a in arr:
        l = list(a)
        l1 = int("".join(["%s0" % x for x in l]))
        l2 = int("".join(["0%s" % x for x in l]))
        s = (s + l1 * n) % b
        s = (s + l2 * n) % b

    print(s)
