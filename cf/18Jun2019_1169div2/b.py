from sys import stdin

if __name__ == '__main__':
    n, m = map(int, input().split())
    lines = stdin.readlines()

    arr = []
    for line in lines:
        a, b = map(int, line.split())
        arr.append((a, b))


    def check(x, arr):
        b = set()
        for a in arr:
            if x not in a:
                if not b:
                    b = set(a)
                elif not b.intersection(set(a)):
                    return False
                else:
                    b = b.intersection(set(a))
        return True


    if check(arr[0][0], arr):
        print("YES")
    elif check(arr[0][1], arr):
        print("YES")
    else:
        print("NO")



