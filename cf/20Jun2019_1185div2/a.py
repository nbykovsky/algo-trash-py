
if __name__ == '__main__':
    a, b, c, d = map(int, input().split())
    delta = map(lambda x: max(0, d - x), sorted([abs(a-b), abs(a-c), abs(b-c)])[:-1])
    print(sum(delta))