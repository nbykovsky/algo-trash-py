import sys

if __name__ == '__main__':
    n = int(input())
    lines = sys.stdin.readlines()
    for i in range(n):
        def cnt(line):
            l1 = []
            for l in line:
                if l1 and l1[-1][0] == l:
                    l1[-1][1] += 1
                else:
                    l1.append([l, 1])
            return l1
        c1 = cnt(lines[i * 2].strip())
        c2 = cnt(lines[i * 2 + 1].strip())
        if len(c1) != len(c2):
            print("NO")
        else:
            good = True
            for p in zip(c1, c2):
                if p[0][0] != p[1][0] or p[0][1] > p[1][1]:
                    good = False
                    break
            if good:
                print("YES")
            else:
                print("NO")
