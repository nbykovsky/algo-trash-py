
if __name__ == '__main__':
    t = int(input())
    for _ in range(t):
        n, m = map(int, input().split())
        arr = []
        for _ in range(n):
            arr.append(list(input().strip()))

        snakes = {}
        layers = {}
        for r in range(n):
            snakes_tmp = {}
            stake = []
            for c in range(m):
                if arr[r][c] == '.':
                    stake.append((c, '.'))
                elif arr[r][c] in snakes_tmp:
                    snakes_tmp[arr[r][c]][1] = c
                else:
                    snakes_tmp[arr[r][c]] = [c, c]

            for chr, pair in snakes_tmp.items():
                stake.append((chr, pair[0]))
                stake.append((chr.upper(), pair[1]))