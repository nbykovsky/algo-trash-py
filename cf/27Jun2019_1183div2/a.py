
if __name__ == '__main__':
    a = int(input())
    for i in range(a, 10**19):
        if sum(map(int, list(str(i)))) % 4 == 0:
            print(i)
            exit(0)