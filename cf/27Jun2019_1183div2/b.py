
if __name__ == '__main__':
    q = int(input())
    for _ in range(q):
        n, k = map(int, input().split())
        aa = list(map(int, input().split()))
        mn, mx = min(aa), max(aa)
        ans = mn + k
        if mx - k > ans:
            print(-1)
        else:
            print(ans)