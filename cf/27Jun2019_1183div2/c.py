from sys import stdin


def func1(k, n, a, b):
    if k <= b*n:
        return -1
    l,r = 0, n
    while l <= r:
        m = (l + r) // 2
        x = m * a + (n - m) * b
        if x < k:
            l = m + 1
        else:
            r = m - 1
    return r


def func2(k, n, a, b):
    if k <= b*n:
        return -1
    else:
        return min(n, (k - n * b - 1) // (a - b))


if __name__ == '__main__':
    q = int(input())
    lines = stdin.readlines()
    for line in lines:
        k,n,a,b = map(int, line.split())
        print(func2(k, n, a, b))