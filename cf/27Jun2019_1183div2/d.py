from sys import stdin
from collections import Counter


if __name__ == '__main__':
    q = int(input())
    lines = stdin.readlines()
    ans = []
    for i in range(q):
        n = int(lines[i*2])
        aa = map(int, lines[i*2 + 1].split())
        cnt = list(Counter(aa).values())
        cnt.sort(reverse=True)
        x = 10**19
        s = 0
        for c in cnt:
            x = max(0, min(x-1, c))
            s += x
        ans.append(str(s))

    print('\n'.join(ans))
