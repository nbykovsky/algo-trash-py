from sys import stdin
from heapq import heappush, heappop

if __name__ == '__main__':
    q = int(input())
    lines = stdin.readlines()
    ans = []
    j = 0
    for _ in range(q):
        n = int(lines[j])
        cnt = {}
        j += 1
        for t in range(n):
            a, f = map(int, lines[j].split())
            if a not in cnt:
                cnt[a] = [1, f]
            else:
                cnt[a][0] += 1
                cnt[a][1] += f

            j += 1
        vls = list(cnt.values())
        vls.sort(reverse=True)
        hp = []
        x = 10**19
        s = 0
        fs = 0
        i = 0
        while x > 0 and (i < len(vls) or hp):
            x = x - 1 if hp else min(x - 1, vls[i][0])

            while i < len(vls) and x <= vls[i][0]:
                heappush(hp, ((-1)*vls[i][1], vls[i][0]))
                i += 1

            f, _ = heappop(hp)
            s += x
            fs += min((-1)*f, x)

        ans.append( "%s %s" % (s, fs))

    print('\n'.join(ans))
