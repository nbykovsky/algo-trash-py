
if __name__ == '__main__':
    x = int(input())
    for _ in range(x):
        n, s, t = map(int, input().split())
        if n == s == t:
            print(1)
        else:
            f = n - s
            g = n - t
            print(max(f, g) + 1)