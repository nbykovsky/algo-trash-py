from sys import stdin
from collections import Counter

if __name__ == '__main__':
    n = int(input())
    ss = input()
    m = int(input())
    ts = list(map(lambda x: x.strip(), stdin.readlines()))
    d = {}
    for i, s in enumerate(ss):
        if s not in d.keys():
            d[s] = [i + 1]
        else:
            d[s].append(i + 1)

    ans = []
    for t in ts:
        mx = 0
        cnt = Counter(t)
        for k, v in cnt.items():
            mx = max(mx, d[k][v-1])
        ans.append(str(mx))

    print("\n".join(ans))