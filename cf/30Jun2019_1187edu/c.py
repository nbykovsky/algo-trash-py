
if __name__ == '__main__':
    n, m = map(int, input().split())
    srt = []
    nsrt = []
    for i in range(m):
        t, l, r = map(int, input().split())
        if t == 1:
            srt.append((l - 1, r - 1))
        else:
            nsrt.append((l - 1, r - 1))

    ans = []
    for i in range(n, 0, -1):
        ans.append(i)

    if not srt:
        print("YES")
        print(" ".join(list(map(str, ans))))
        exit(0)

    srt.sort()
    inters = []
    beg, end = srt[0]
    for l, r in srt:
        if l <= end:
            end = max(end, r)
        else:
            inters.append((beg, 0, end))
            beg, end = l, r
    inters.append((beg, 0, end))

    for l, r in nsrt:
        inters.append((l, 1, r))

    inters.sort()

    b1, e1 = -1, -1
    for l, o, r in inters:
        if o == 0:
            b1, e1 = l, r
            tmp = ans[l:r + 1]
            tmp.sort()
            ans[l:r + 1] = tmp
        else:
            if b1 != -1 and e1 != -1 and b1 <= l and r <= e1:
                print("NO")
                exit(0)

    print("YES")
    print(" ".join(list(map(str, ans))))
