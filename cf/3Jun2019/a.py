

if __name__ == '__main__':
    n = int(input())
    aa = list(map(int, input().split()))
    aa.sort()
    if aa[0] == aa[-1]:
        print(-1)
    else:
        print(" ".join(map(str, aa)))