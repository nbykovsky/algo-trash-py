

if __name__ == '__main__':
    n, x = map(int, input().split())

    r = []
    d = set()
    for a in range(1, 2**n):
        tmp = x ^ a
        if (tmp not in d) and (a not in d) and a not in (0, x):
            r.append(a)
            d.add(a)

    if not r:
        print(0)
    else:
        res = [r[0]]
        for i in range(1, len(r)):
            res.append(r[i-1] ^ r[i])

        print(len(res))
        print(" ".join(map(str, res)))
