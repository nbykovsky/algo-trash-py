

if __name__ == '__main__':
    t = int(input())
    for _ in range(t):
        n, k = map(int, input().split())
        cnt = 0
        while n > 0:
            cnt += n % k
            n //= k
            if n > 0:
                cnt += 1
        print(cnt)


