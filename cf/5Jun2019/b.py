from collections import deque

base = 2 ** 32 - 1

if __name__ == '__main__':
    n = int(input())
    s = deque()
    s.append(1)
    acc = 0
    for _ in range(n):
        cmd = input()
        if cmd == 'add':
            acc += s[-1]
            if acc > base:
                print("OVERFLOW!!!")
                exit(0)
        elif cmd == 'end':
            s.pop()
        else:
            num = int(cmd[4:])
            nxt = s[-1]*num
            if nxt > base:
                nxt = base + 1
            s.append(nxt)

    print(acc)