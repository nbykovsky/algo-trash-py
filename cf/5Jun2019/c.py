import sys

inf = 10 ** 9 + 1

if __name__ == '__main__':
    t = int(input())
    res = []
    lines = sys.stdin.readlines()
    for e in range(t):
        n, k = map(int, lines[e*2].split())
        aa = list(map(int, lines[e*2 + 1].split()))

        fx = inf
        x = None
        for i in range(k, n):
            x_tmp = (aa[i - k] + aa[i]) >> 1
            fx_tmp = max(abs(aa[i-k] - x_tmp), abs(aa[i] - x_tmp))
            if fx_tmp < fx:
                x = x_tmp
                fx = fx_tmp
        res.append(str(x))

    print('\n'.join(res))
