# problem http://codeforces.com/contest/1092/problem/A
from math import ceil


def get_string(n, k):
    base = "".join(map(chr, range(97, 97 + k)))
    result = ""
    for _ in range(ceil(n / k)):
        result += base

    return result[:n]


if __name__ == '__main__':
    for _ in range(0, int(input())):
        n, k = map(int, input().split())
        print(get_string(n, k))
