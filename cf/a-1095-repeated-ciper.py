# problem http://codeforces.com/contest/1095/my


def decrypt(s):

    res = ""
    i_res = 1
    i_s = 0
    while i_s != len(s):
        res += s[i_s]
        i_s += i_res
        i_res += 1
    return res


if __name__ == '__main__':
    n = int(input())
    print(decrypt(input()))
