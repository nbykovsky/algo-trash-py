# problem http://codeforces.com/contest/1100/problem/A

def calc(a, k, b):

    for i in range(b, len(a), k):
        a[i] = 0
    e = sum([1 for x in a if x == 1])
    s = sum([1 for x in a if x == -1])
    return abs(e - s)


if __name__ == '__main__':
    n, k = map(int, input().split())
    a = list(map(int, input().split()))
    m = 0
    for b in range(0, k):
        m = max(m, calc(a[:], k, b))

    print(m)

