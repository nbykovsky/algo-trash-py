# problem http://codeforces.com/contest/1102/problem/A

if __name__ == '__main__':
    n = int(input())
    if n == 1:
        print(1)
    elif (n * (n + 1) / 2) % 2 == 1:
        print(1)
    else:
        print(0)