# problem https://codeforces.com/contest/1105/problem/A


def cnt(a, t):
    s = 0
    for aa in a:
        if aa < t - 1:
            s += t - 1 - aa
        elif aa > t + 1:
            s += aa - (t + 1)
    return s


if __name__ == '__main__':
    n = int(input())
    a = list(map(int, input().split()))

    mn = 10 ** 19
    tt = 0
    for t in range(1, 101):
        ss = cnt(a, t)
        if ss < mn:
            mn = ss
            tt = t

    print(tt, mn)
