# problem https://codeforces.com/contest/1106/problem/A


if __name__ == '__main__':
    n = int(input())
    mtr = []
    for _ in range(n):
        mtr.append(input().strip())

    cnt = 0
    for r in range(1, n - 1):
        for c in range(1, n - 1):
            if mtr[r][c] == mtr[r - 1][c - 1] == mtr[r - 1][c + 1] == mtr[r + 1][c - 1] == mtr[r + 1][c + 1] == 'X':
                cnt += 1

    print(cnt)
