# problem https://codeforces.com/contest/1108/problem/A



if __name__ == '__main__':
    q = int(input())
    for _ in range(q):
        l1, r1, l2, r2 = map(int, input().split())
        if l1 != r2:
            print(l1, r2)
        else:
            print(r1, l2)