# problem https://codeforces.com/contest/1111/problem/A


g = ['a', 'e', 'i', 'o', 'u']

if __name__ == '__main__':
    ss = input()
    tt = input()
    if len(ss) != len(tt):
        print("No")
        exit(0)
    for s, t in zip(ss, tt):
        x = s in g
        y = t in g
        if x ^ y:
            print("No")
            exit(0)
    print("Yes")
