# problem http://codeforces.com/problemset/problem/1099/A


def calculate(w, h, u1, d1, u2, d2):
    sq = lambda x: int(x * (x + 1) / 2)
    before_1_stone = w + sq(h) - sq(d1 - 1)
    before_2_stone = max(before_1_stone - u1, 0) + sq(d1 - 1) - sq(d2 - 1)
    finish = max(before_2_stone - u2, 0) + sq(d2 - 1)
    return finish


if __name__ == '__main__':
    w, h = map(int, input().split())
    u1, d1 = map(int, input().split())
    u2, d2 = map(int, input().split())
    if d1 < d2:
        d1, d2 = d2, d1
        u1, u2 = u2, u1
    print(calculate(w, h, u1, d1, u2, d2))

