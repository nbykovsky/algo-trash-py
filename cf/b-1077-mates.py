# problem http://codeforces.com/contest/1077/problem/B


if __name__ == '__main__':
    n = int(input())
    arr = input().strip().split()
    st = "".join(arr)
    k = 0
    while st.find('10101') != -1:
        st = st.replace('10101', '10001', 1)
        k += 1

    while st.find('101') != -1:
        st = st.replace('101', '100', 1)
        k += 1

    print(k)
