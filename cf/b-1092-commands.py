# problem http://codeforces.com/contest/1092/problem/B

def teams(arr):
    arr.sort()
    s = 0
    for i in range(0, len(arr), 2):
        s += arr[i + 1] - arr[i]
    return s


if __name__ == "__main__":
    n = int(input())
    arr = list(map(int, input().split()))
    print(teams(arr))
