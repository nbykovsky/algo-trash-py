# problem http://codeforces.com/contest/1095/problem/B


def stab(arr):
    if len(arr) < 3:
        return 0
    arr.sort()
    if arr[1] - arr[0] > arr[-1] - arr[-2]:
        return arr[-1] - arr[1]
    else:
        return arr[-2] - arr[0]


if __name__ == '__main__':
    n = int(input())
    arr = list(map(int, input().strip().split()))
    print(stab(arr))
