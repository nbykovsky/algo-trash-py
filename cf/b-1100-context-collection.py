# problem http://codeforces.com/contest/1100/problem/B

if __name__ == '__main__':
    n, m = map(int, input().split())
    aa = list(map(int, input().split()))
    t = dict()
    result = []
    for a in aa:
        if a in t.keys():
            t[a] += 1
            result.append('0')
        else:
            t[a] = 1
            if len(t) == n:
                result.append('1')
                for k in range(1, n + 1):
                    if t[k] == 1:
                        del t[k]
                    else:
                        t[k] -= 1
            else:
                result.append('0')

    print("".join(result))
