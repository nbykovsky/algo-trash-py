# problem http://codeforces.com/contest/1101/problem/B
import re

if __name__ == '__main__':
    st = input()
    if st.find("[", 0) < 0 or st.rfind(']') < 0 or st.find("[", 0) > st.rfind(']'):
        print(-1)
        exit(0)
    st = st[st.find("[", 0):st.rfind(']')]
    if st.find(':') == st.rfind(':'):
        print(-1)
        exit(0)

    st = st[st.find(":", 0):st.rfind(':')]
    print(len(re.sub(r"[^|]", "", st)) + 4)
