# problem http://codeforces.com/contest/1102/problem/B

def calc(n, k, arr):
    not_used = set(range(1, k+1))
    all = set(range(1, k+1))
    d = [set() for i in range(5001)]
    res = []
    for a in arr:
        if len(d[a]) <= k:
            c = not_used - d[a]
            if c:
                res.append(list(c)[0])
                not_used -= {list(c)[0]}
                d[a] |= {list(c)[0]}
            else:
                c1 = all - d[a]
                if c1:
                    res.append(list(c1)[0])
                    d[a] |= {list(c1)[0]}
                else:
                    return []

        else:
            return []

    return res


if __name__ == '__main__':
    n, k = map(int, input().split())
    arr = map(int, input().split())
    r = calc(n, k, arr)
    if r:
        print("YES")
        print(" ".join(list(map(str, r))))
    else:
        print("NO")