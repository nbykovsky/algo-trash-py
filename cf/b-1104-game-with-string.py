# problem https://codeforces.com/contest/1104/problem/B

from collections import deque

if __name__ == '__main__':
    s = list(input())
    queue = deque()
    b = False
    for x in s:
        if not queue:
            queue.append(x)
        elif x == queue[-1]:
            b = not b
            queue.pop()
        else:
            queue.append(x)

    if not b:
        print("No")
    else:
        print("Yes")