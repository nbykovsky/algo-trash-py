# problem https://codeforces.com/contest/1105/problem/B


if __name__ == '__main__':
    n, k = map(int, input().split())
    s = input()
    d = {}
    prev = 'na'
    for sym in s:
        if sym in d.keys():
            if sym == prev:
                d[sym][-1] += 1
            else:
                d[sym].append(1)
        else:
            d[sym] = [1]
        prev = sym

    mx = 0
    for _, v in d.items():
        tmp = 0
        for x in v:
            tmp += x // k
        mx = max(mx, tmp)

    print(mx)
