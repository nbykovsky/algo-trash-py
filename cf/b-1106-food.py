# problem https://codeforces.com/contest/1106/problem/B


import sys

if __name__ == '__main__':
    n, m = map(int, input().split())
    a = list(map(int, input().split()))
    c = list(map(int, input().split()))

    lines = sys.stdin.readlines()

    tmp = list(zip(c, range(n)))
    tmp.sort()
    cheapest = 0
    result = []
    for idx in range(m):
        t, d = map(int, lines[idx].split())
        t -= 1

        score = 0
        while cheapest < n and d != 0:
            if a[t] >= d:
                score += c[t] * d
                a[t] -= d
                d = 0
            elif 0 < a[t] < d:
                score += c[t] * a[t]
                d -= a[t]
                a[t] = 0
            elif a[t] == 0:
                while cheapest < n and a[tmp[cheapest][1]] == 0:
                    cheapest += 1
                if cheapest == n:
                    break
                elif a[tmp[cheapest][1]] >= d:
                    score += c[tmp[cheapest][1]] * d
                    a[tmp[cheapest][1]] -= d
                    d = 0
                elif 0 < a[tmp[cheapest][1]] < d:
                    score += c[tmp[cheapest][1]] * a[tmp[cheapest][1]]
                    d -= a[tmp[cheapest][1]]
                    a[tmp[cheapest][1]] = 0
                else:
                    raise Exception("test0")

            else:
                raise Exception("test")
        if cheapest < n:
            result.append(score)
        else:
            result.append(0)

    print("\n".join(list(map(str, result))))



