# problem https://codeforces.com/contest/1107/problem/B


if __name__ == '__main__':
    n = int(input())

    for i in range(n):
        k, x = map(int, input().split())
        print(9 * (k - 1) + x)
