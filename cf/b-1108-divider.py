# problem https://codeforces.com/contest/1108/problem/B

if __name__ == '__main__':
    n = int(input())
    d = list(map(int, input().split()))
    d.sort(reverse=True)
    a = d[0]
    arr = set()
    for i in range(1, a):
        if a % i == 0:
            arr.add(i)

    other = []
    for j in d[1:]:
        if j in arr:
            arr.remove(j)
        else:
            other.append(j)

    other.sort()
    print(a, other[-1])




