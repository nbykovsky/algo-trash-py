# problem https://codeforces.com/contest/1111/problem/b


if __name__ == '__main__':
    n, k, m = map(int, input().split())
    aa = list(map(int, input().split()))
    aa.sort()
    s = sum(aa)
    mx = (s + min(m, n*k)) / n
    for start in range(min(n - 1, m)):
        s -= aa[start]
        tmp = s + min(m-start - 1, (n-start - 1)*k)
        mx = max(mx, tmp / (n - start - 1))

    print(mx)


