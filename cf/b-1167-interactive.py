# https://codeforces.com/contest/1167/problem/B
import sys


lst = [4, 8, 15, 16, 23, 42]
if __name__ == '__main__':
    st = {}
    for a in lst:
        for b in lst:
            if a > b:
                st[a*b] = {a, b}

    ans = []
    for i in range(4):
        print('?  %s %s' % (i + 1, i + 2))
        sys.stdout.flush()
        ans.append(st[int(input())])

    res = []
    res.append(list(ans[0] - ans[1])[0])
    res.append(list(ans[1] - ans[2])[0])
    res.append(list(ans[2] - ans[3])[0])
    res.append(list(ans[2] - {res[-1]})[0])
    res.append(list(ans[3] - {res[-1]})[0])
    res.append(list(set(lst) - set(res))[0])

    print("! " + " ".join(map(str, res)))
    sys.stdout.flush()



"""
?  1 2
32
?  2 3
120
?  3 4
240
?  4 5
368
"""