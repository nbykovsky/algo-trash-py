# problem http://codeforces.com/contest/1099/problem/B
from math import floor, ceil, sqrt


def calc(n):
    if floor(sqrt(n)) ** 2 == n:
        return int(2 * sqrt(n))
    else:
        return int(2 * floor(sqrt(n)) + 1)


def max_num_of_squires(n):
    return floor(n/2) * ceil(n/2)


def search(f, t, v):
    if f + 1 == t:
        return t

    mid = floor((f + t) / 2)
    if v <= max_num_of_squires(mid):
        return search(f, mid, v)
    else:
        return search(mid, t, v)


if __name__ == '__main__':
    num = int(input())
    print(int(search(0, ceil(sqrt(num)) * 2, num)))
