# problem http://codeforces.com/contest/1077/problem/C


if __name__ == '__main__':
    n = int(input())
    a = list(map(int, input().split()))
    s = sum(a)
    a = [(b, i) for i, b in enumerate(a)]
    a.sort()
    s1 = s - a[-1][0]
    res = []
    for i in range(n - 1):
        if s1 - a[i][0] == a[-1][0]:
            res.append(str(a[i][1] + 1))

    if a[-2][0] == s - a[-2][0] - a[-1][0]:
        res.append(str(a[-1][1] + 1))

    print(len(res))
    print(" ".join(res))
