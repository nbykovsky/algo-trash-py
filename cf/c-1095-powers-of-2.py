# problem http://codeforces.com/contest/1095/problem/C


def init_split(n):

    ind = 1
    mask = ~0
    res = []
    while n != 0:
        if (n & ind) != 0:
            res.append((n & ind))
        ind <<= 1
        mask <<= 1
        n &= mask
    return res


def split(n, k):
    init = list(reversed(init_split(n)))
    if len(init) > k:
        return []

    offset = k - len(init)
    while offset != 0:
        if init[0] == 1:
            return []
        elif init[0] < offset + 1:
            init.extend([1]*init[0])
            offset -= (init[0] - 1)
            del init[0]
        else:
            init[:1] = [int(init[0] / 2)] * 2
            offset -= 1

    return init


if __name__ == '__main__':
    n, k = map(int, input().split())
    res = split(n, k)
    if not res:
        print("NO")
    else:
        print("YES")
        print(" ".join(map(str, res)))
