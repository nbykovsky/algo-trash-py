# problem http://codeforces.com/contest/1100/problem/C

import math

if __name__ == '__main__':
    n, r = map(int, input().split())
    R = r * math.sin(math.pi /n) / (1 - math.sin((math.pi) /n))
    print(R)