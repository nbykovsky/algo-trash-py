

if __name__ == '__main__':
    t = int(input())
    for _ in range(t):
        n = int(input())
        x = []
        for i in range(n):
            l, r = map(int, input().split())
            x.append((l, r, i))
        x.sort()
        e = x[0][0]
        g = ["2"] * n
        done = False
        res = ""
        for y in x:
            if e < y[0]:
                done = True
                break
            e = max(e, y[1])
            g[y[2]] = "1"
        if not done:
            print(-1)
        else:
            print(" ".join(g))

