# problem https://codeforces.com/contest/1104/problem/C


if __name__ == '__main__':
    s = list(input())
    ans = []
    vert = False
    hor = False
    for x in s:
        if x == '0' and not vert:
            vert = True
            ans.append((1, 1))
        elif x == '0':
            vert = False
            ans.append((3, 1))
        elif x == '1' and not hor:
            hor = True
            ans.append((4, 3))
        elif x == '1':
            hor = False
            ans.append((4, 1))

    for a, b in ans:
        print(a, b)