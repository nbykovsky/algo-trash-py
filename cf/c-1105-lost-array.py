# problem https://codeforces.com/contest/1105/problem/C

import math

base = 10 ** 9 + 7

if __name__ == '__main__':
    n, l, r = map(int, input().split())
    dp = [[-1] * 3 for _ in range(n)]
    dp[0][0] = math.floor(r/3) - math.ceil(l/3) + 1
    dp[0][1] = math.floor((r - 1)/3) - math.ceil((l - 1)/3) + 1
    dp[0][2] = math.floor((r - 2)/3) - math.ceil((l - 2)/3) + 1
    for i in range(1, n):
        dp[i][0] = (dp[i - 1][0] * dp[0][0] + dp[i - 1][1] * dp[0][2] + dp[i - 1][2] * dp[0][1]) % base
        dp[i][1] = (dp[i - 1][1] * dp[0][0] + dp[i - 1][0] * dp[0][1] + dp[i - 1][2] * dp[0][2]) % base
        dp[i][2] = (dp[i - 1][1] * dp[0][1] + dp[i - 1][0] * dp[0][2] + dp[i - 1][2] * dp[0][0]) % base

    print(dp[n - 1][0])
