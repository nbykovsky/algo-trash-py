# problem https://codeforces.com/contest/1106/problem/C


if __name__ == '__main__':
    n = int(input())
    a = list(map(int, input().split()))
    a.sort()
    s = 0
    for i in range(n//2):
        s += (a[i] + a[-(i+1)]) ** 2
    print(s)