# problem https://codeforces.com/contest/1107/problem/C


if __name__ == '__main__':
    n, k = map(int, input().split())
    aa = list(map(int, input().split()))
    ss = list(input())

    grp = [[aa[0]]]
    for i in range(1, n):
        if ss[i] == ss[i - 1]:
            grp[-1].append(aa[i])
        else:
            grp.append([aa[i]])

    sm = 0
    for i in range(len(grp)):
        grp[i].sort()
        sm += sum(grp[i][-k:])

    print(sm)
