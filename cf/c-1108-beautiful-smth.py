# problem https://codeforces.com/contest/1108/problem/C


if __name__ == '__main__':
    n = int(input())
    s = input()
    s1 = ("RGB" * (n // 3 + 1))[:n]
    s2 = ("GBR" * (n // 3 + 1))[:n]
    s3 = ("BRG" * (n // 3 + 1))[:n]
    s4 = ("BGR" * (n // 3 + 1))[:n]
    s5 = ("RBG" * (n // 3 + 1))[:n]
    s6 = ("GRB" * (n // 3 + 1))[:n]

    m1 = 0
    m2 = 0
    m3 = 0
    m4 = 0
    m5 = 0
    m6 = 0
    for i in range(n):
        if s[i] != s1[i]: m1 += 1
        if s[i] != s2[i]: m2 += 1
        if s[i] != s3[i]: m3 += 1
        if s[i] != s4[i]: m4 += 1
        if s[i] != s5[i]: m5 += 1
        if s[i] != s6[i]: m6 += 1

    if m1 == min(m1, m2, m3, m4, m5, m6):
        print(m1)
        print(s1)
    elif m2 == min(m1, m2, m3, m4, m5, m6):
        print(m2)
        print(s2)
    elif m3 == min(m1, m2, m3, m4, m5, m6):
        print(m3)
        print(s3)
    elif m4 == min(m1, m2, m3, m4, m5, m6):
        print(m4)
        print(s4)
    elif m5 == min(m1, m2, m3, m4, m5, m6):
        print(m5)
        print(s5)
    elif m6 == min(m1, m2, m3, m4, m5, m6):
        print(m6)
        print(s6)