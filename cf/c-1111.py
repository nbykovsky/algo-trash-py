# problem https://codeforces.com/contest/1111/problem/C


if __name__ == '__main__':
    n, k, A, B = map(int, input().split())
    aa = map(int, input().split())
    al = {}
    for a in aa:
        if a in al.keys():
            al[a - 1] += B
        else:
            al[a - 1] = B

    # while 0 not in al.keys() or (0 in al.keys() and len(al) > 1):
    for _ in range(n):
        al_tmp = {}
        ps = set()
        for k, v in al.items():
            k >>= 1
            if k in al_tmp.keys():
                al_tmp[k] += v
                ps.remove(k)
            else:
                al_tmp[k] = v
                ps.add(k)

        for p in ps:
            al_tmp[p] = min(A + al_tmp[p], al_tmp[p] * 4)

        al = al_tmp

    print(list(al.values())[0])


"""
3 2 5 1
7 8
"""