# problem https://codeforces.com/contest/1148/problem/C


if __name__ == '__main__':
    n = int(input())
    ps = list(map(int, input().split()))
    res = []
    i = 0
    while i < n:
        j = ps[i] - 1
        if i == j:
            i += 1
            continue
        elif abs(i - j) >= n // 2:
            res.append((i + 1 ,j + 1))
            ps[i], ps[j] = ps[j], ps[i]
        elif i + 1 <= n // 2 and j + 1 <= n // 2:
            res.append((i + 1, n - 1 + 1))
            res.append((j + 1, n - 1 + 1))
            res.append((i + 1, n - 1 + 1))
            ps[i], ps[n - 1] = ps[n - 1], ps[i]
            ps[j], ps[n - 1] = ps[n - 1], ps[j]
            ps[i], ps[n - 1] = ps[n - 1], ps[i]
        elif i + 1 > n // 2 and j + 1 > n // 2:
            res.append((i + 1, 1))
            res.append((j + 1, 1))
            res.append((i + 1, 1))
            ps[i], ps[0] = ps[0], ps[i]
            ps[j], ps[0] = ps[0], ps[j]
            ps[i], ps[0] = ps[0], ps[i]
        else:
            i1 = max(i, j)
            j1 = min(i, j)
            res.append((j1 + 1, n))
            ps[j1], ps[n - 1] = ps[n - 1], ps[j1]
            res.append((i1 + 1, 1))
            ps[i1], ps[0] = ps[0], ps[i1]
            res.append((1, n))
            ps[0], ps[n - 1] = ps[n - 1], ps[0]
            res.append((j1 + 1, n))
            ps[j1], ps[n - 1] = ps[n - 1], ps[j1]
            res.append((i1 + 1, 1))
            ps[i1], ps[0] = ps[0], ps[i1]

    print(len(res))
    for a,b in res:
        print(a,b)
