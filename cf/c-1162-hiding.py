# problem https://codeforces.com/contest/1162/problem/C

if __name__ == '__main__':
    n, k = map(int, input().split())
    xs = list(map(int, input().split()))
    st = {}
    pairs = set()
    for x in xs:
        if x in st.keys():
            st[x] += 1
        else:
            st[x] = 1

        pairs.add((x, x))

    for x in xs:
        if x+1 in st.keys() and st[x+1] > 0:
            pairs.add((x, x+1))

        if x-1 in st.keys() and st[x-1] > 0:
            pairs.add((x, x-1))

        st[x] -= 1

    print(2*(n - 1) + n - len(pairs))