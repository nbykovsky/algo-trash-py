# https://codeforces.com/contest/1167/problem/C
from collections import Counter
from collections import defaultdict
import sys

def get_root(arr, i):
    j = i
    path = []
    while arr[j] != j:
        path.append(j)
        j = arr[j]
    for p in path:
        arr[p] = j
    return j


if __name__ == '__main__':
    n, m = map(int, input().split())
    arr = list(range(n))
    lines = sys.stdin.readlines()
    for line in lines:
        gs = list(map(lambda x: int(x) - 1, line.split()))[1:]
        if len(gs) == 0:
            continue
        prev = gs[0]
        i1 = get_root(arr, prev)
        for curr in gs[1:]:
            # i1 = get_root(arr, prev)
            i2 = get_root(arr, curr)
            if i1 != i2:
                arr[i2] = arr[i1]
                # prev = curr

    output = [0] * n
    for i in range(n):
        r = get_root(arr, i)
        output[r] += 1

        # d = dict(Counter(arr))

    print(" ".join(map(lambda x: str(output[x]), arr)))