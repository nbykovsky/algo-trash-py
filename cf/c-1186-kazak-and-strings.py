# problem https://codeforces.com/contest/1186/problem/C

if __name__ == '__main__':
    a = list(map(int, list(input())))
    b = list(map(int, list(input())))
    n = len(b)
    acc_b = sum(b) % 2
    acc_a = sum(a[:n]) % 2
    cnt = 0 if acc_a ^ acc_b else 1
    for i in range(n, len(a)):
        acc_a = acc_a ^ a[i] ^ a[i - n]
        cnt += 0 if acc_a ^ acc_b else 1
    print(cnt)