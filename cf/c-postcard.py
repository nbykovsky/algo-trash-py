# problem http://codeforces.com/contest/1099/problem/C
import re


def get_string(s, k):
    s = list(s)
    # getting min length
    min_length = len(s) - 2 * len(list([1 for c in s if c in ('?', '*')]))
    if min_length > k:
        return "Impossible"
    if min_length < k:
        pos = 0
        for i in range(min_length, k):
            while pos < len(s) and s[pos] not in ('?', '*'):
                pos += 1

            if pos == len(s):
                return "Impossible"
            if s[pos] == '?':
                del s[pos]
            elif s[pos] == '*':
                s[pos:pos+1] = [s[pos - 1]] * (k - i - 1)
                break

    return re.sub(r"[a-z]\?|[a-z]\*", "", "".join(s))


if __name__ == '__main__':
    s = input().strip()
    k = int(input())
    print(get_string(s, k))
