# problem http://codeforces.com/contest/1077/problem/D


from collections import Counter


def can_cut(t, nm, k):
    total = 0
    for cnt, num in t:
        if cnt // nm == 0:
            return False
        total += cnt // nm
        if total >= k:
            return True


if __name__ == '__main__':
    n, k = map(int, input().split())
    s = list(map(int, input().split()))
    c = Counter(s)
    t = [(b, a) for a, b in c.items()]
    t.sort(reverse=True)

    l, r = 0, t[0][0] + 1
    while l + 1 != r:
        m = (l + r) // 2
        if not can_cut(t, m, k):
            r = m
        else:
            l = m

    arr = []
    for p in t:
        arr.extend([str(p[1])]*(p[0] // l))

    print(" ".join(arr[:k]))