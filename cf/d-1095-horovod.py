# http://codeforces.com/contest/1095/problem/D


if __name__ == '__main__':
    n = int(input())
    arr = [None] * n
    for i in range(n):
        a, b = map(int, input().split())
        a -= 1
        b -= 1
        arr[i] = [a, b]

    if n < 4:
        print(" ".join(list(map(str, range(1, n + 1)))))
        exit(0)

    result = [None] * n
    for i, p in enumerate(arr):
        if p[1] in arr[p[0]]:
            result[i] = p[0]
        else:
            result[i] = p[1]

    idx = result[0]
    while idx != 0:
        print(idx + 1, end=" ")
        idx = result[idx]

    print(1)
