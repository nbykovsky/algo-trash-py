# problem https://codeforces.com/contest/1108/problem/D


if __name__ == '__main__':
    n = int(input())
    s = input()

    dp = [[10**19, 10**19, 10**19] for _ in range(n)]

    prev = [[-1]*3 for _ in range(n)]

    dct = {
        'R':0,
        'B':1,
        'G':2
    }

    back = {
        0: 'R',
        1: 'B',
        2: 'G'
    }

    l = list(map(lambda x: dct[x], s))

    dp[0][0] = 1
    dp[0][1] = 1
    dp[0][2] = 1
    dp[0][l[0]] = 0

    for i in range(1, len(s)):
        for j1 in range(0, 3):
            for j0 in range(0, 3):
                if j1 == j0:
                    continue
                add = 0 if l[i] == j1 else 1
                if dp[i - 1][j0] + add < dp[i][j1]:
                    dp[i][j1] = dp[i - 1][j0] + add
                    prev[i][j1] = j0

    tmp = [
        (dp[-1][0], 0),
        (dp[-1][1], 1),
        (dp[-1][2], 2)
    ]
    tmp.sort()
    min_number, nxt = tmp[0]

    result = [nxt]
    for p in reversed(prev[1:]):
        nxt = p[nxt]
        result.append(nxt)

    print(min_number)
    print("".join(list(map(lambda x: back[x], reversed(result)))))



#RBGRGBRGR
#RBGBRBRGR