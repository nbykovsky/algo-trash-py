
if __name__ == '__main__':
    n = int(input())
    ss = list(input())
    r = 0
    b = 0
    res = []
    for s in ss:
        if s == '(':
            if r > b:
                res.append('0')
                b += 1
            else:
                res.append('1')
                r += 1
        else:
            if r > b:
                res.append('1')
                r -= 1
            else:
                res.append('0')
                b -= 1

    print("".join(res))
