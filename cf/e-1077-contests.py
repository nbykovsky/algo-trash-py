# problem http://codeforces.com/contest/1077/problem/E


from collections import Counter

if __name__ == '__main__':
    n = int(input())
    a = list(map(int, input().split()))
    cnt = Counter(a)
    arr = [b for _, b in cnt.items()]
    arr.sort()
    if len(arr) == 1:
        print(arr[0])
        exit(0)

    mx = 0
    for i in range(1, arr[-1] + 1):
        j = i
        mx_tmp = j
        idx = len(arr) - 2
        while j & 1 == 0 and idx > -1:
            j >>= 1
            if j <= arr[idx]:
                mx_tmp += j
                idx -= 1
            else:
                break
        mx = max(mx, mx_tmp)

    print(mx)
