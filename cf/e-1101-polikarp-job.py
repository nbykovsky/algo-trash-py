import sys

if __name__ == '__main__':
    n = int(input())
    m_x = 0
    m_y = 0
    lines = sys.stdin.readlines()
    for line in lines:
        s, a, b = line.split()
        a = int(a)
        b = int(b)
        if s == '+':
            if b > a:
                m_x = max(a, m_x)
                m_y = max(b, m_y)
            else:
                m_x = max(b, m_x)
                m_y = max(a, m_y)
        else:
            if (a >= m_x and b >= m_y) or (b >= m_x and a >= m_y):
                print("YES")
            else:
                print("NO")

