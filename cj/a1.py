

def max_suf(w1, w2):
    e = min(len(w1), len(w2))
    for i in range(e):
        if w1[i] != w2[i]:
            return ''.join(w1[:i])

    return ''.join(w1[:e])


if __name__ == '__main__':
    t = int(input())

    for i in range(1, t + 1):
        n = int(input())
        ws = []
        for j in range(n):
          w = list(reversed(input()))
          ws.append(w)
        ws.sort()

        j = 1
        suff_prev = ""
        deleted = 0
        found = 0
        while j != len(ws) and j < len(ws):
            ms = max_suf(ws[j], ws[j-1])
            if ms != suff_prev:
                j += 2
                suff_prev = ms
                found += 2
            elif len(ms) > 1:
                j += 2
                suff_prev = suff_prev[:-1]
                found += 2
            else:
                deleted += 1
                j += 1
                suff_prev = ""

        print("Case #%s: %s" % (i, found))
