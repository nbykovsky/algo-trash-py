
if __name__ == '__main__':
    n = int(input())
    for i in range(n):
        l = list(input())
        n1 = ''.join(map(lambda x: '2' if x == '4' else x, l))
        n2 = ''.join(map(lambda x: '0' if x != '4' else '2', l))
        n2 = n2[n2.find('2'):]
        print("Case #%s: %s %s" % (i + 1, n1, n2))