
if __name__ == '__main__':
    n = int(input())
    for i in range(n):
        _ = input()
        p = list(input())
        p1 = ''.join(map(lambda x: 'E' if x == 'S' else 'S', p))
        print("Case #%s: %s" % (i + 1, p1))
