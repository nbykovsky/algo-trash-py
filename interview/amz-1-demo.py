def cellCompete(states, days):
    # WRITE YOUR CODE HERE
    acc = 0
    for i in range(0, 8):
        acc += (states[i] << (7 - i))

    for _ in range(days):
        acc = (((acc << 2) ^ acc) >> 1) & 255

    acc1 = []
    for pos in range(0, 8):
        if acc & (1 << (7 - pos)):
            acc1.append(1)
        else:
            acc1.append(0)
    return acc1


if __name__ == '__main__':
    print(cellCompete([1,0,0,0,0,1,0,0], 1))
    print(cellCompete([1,1,1, 0, 1, 1, 1, 1], 2))