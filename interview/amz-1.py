# 23280666786829
from math import sqrt

def nearestVegetarianRestaurant(totalRestaurants, allLocations, numRestaurants):
    allDist = sorted([(sqrt(p[0] ** 2 + p[1] ** 2), p) for p in allLocations])
    return list(map(lambda x: x[1], allDist[:numRestaurants]))

if __name__ == '__main__':
    print(nearestVegetarianRestaurant(3, [[-1.1, -3.4], [0, 0], [1,2], [3,4]], 1))
    print(nearestVegetarianRestaurant(3, [], 0))

