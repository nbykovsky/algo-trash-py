def generalizedGCD(num, arr):
    # WRITE YOUR CODE HERE
    gcd = 1
    arr = filter(lambda x: x != 0, arr)
    while len(arr) != 1:
        gcd = min(arr)
        arr = [a % gcd for a in arr if a % gcd != 0]
        arr.append(gcd)

    return arr[0]

if __name__ == '__main__':
    print(generalizedGCD(5, [2,3,4,5,6]))
    print(generalizedGCD(5, [2,4,6,8,10]))
    print(generalizedGCD(2, [0, 10000000]))