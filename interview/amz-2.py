from  Queue import Queue


def getGraph(numRows, numColumns, area):
    graph = dict()
    dist = None
    for i in range(numRows):
        for j in range(numColumns):
            if area[i][j] == 9:
                dist = (i, j)
            if area[i][j] > 0:
                a = [
                    ((i,j),(i-1,j)),
                    ((i,j),(i, j-1)),
                    ((i,j),(i+1, j)),
                    ((i,j),(i, j+1)),
                    ]
                for f, t in filter(lambda x: numRows > x[1][0] >= 0 and numColumns > x[1][1] >= 0 and area[x[1][0]][x[1][1]] > 0, a):
                    if f in graph:
                        graph[f].append(t)
                    else:
                        graph[f] = [t]

    return graph, dist


def minimumDistance(numRows, numColumns, area):
    # WRITE YOUR CODE HERE
    graph, target = getGraph(numRows, numColumns, area)
    if target == (0,0):
        return 0
    q = Queue()
    visited = {(0,0)}
    q.put((0,0))
    dist = 0
    while not q.empty():
        el = q.get()
        dist += 1
        for nxt in graph[el]:
            if nxt in visited:
                continue
            visited.add(nxt)
            if nxt == target:
                return dist
            else:
                q.put(nxt)


if __name__ == '__main__':
    a = [
        [1,0,0],
        [1,0,0],
        [1,9,0]
    ]
    # print(getGraph(3,3, a))
    print(minimumDistance(3, 3, a))