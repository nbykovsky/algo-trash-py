from typing import List
from collections import Counter


def numDecodings(s: str) -> int:
    n = len(s)
    dp = [0] * (n + 1)
    dp[0] = 1
    for i in range(1, n + 1):
        if i > 1 and 9 < int(s[i - 2] + s[i - 1]) <= 26:
            dp[i] += dp[i - 2]
        if s[i - 1] != 0:
            dp[i] += dp[i - 1]

    return dp[-1]

if __name__ == '__main__':
    print(numDecodings("100"))