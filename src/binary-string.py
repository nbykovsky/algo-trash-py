# problem https://practice.geeksforgeeks.org/problems/binary-string/0
from functools import reduce


def factorial(n):
    return reduce(lambda x, acc: acc + x, range(1, n + 1), 0)


if __name__ == '__main__':
    t = int(input())
    for _ in range(t):
        _ = input()
        print(factorial(len([x for x in input() if x == "1"]) - 1))
