# problem https://www.hackerrank.com/challenges/components-in-graph/problem
from collections import Counter


def get_top(arr, idx, l):
    return (idx, l) if arr[idx] == idx else get_top(arr, arr[idx], l + 1)


if __name__ == '__main__':
    n = int(input())
    arr = list(range(2 * n))
    for _ in range(n):
        f, t = map(int, input().strip().split())
        f += -1
        t += -1
        (f_top, f_l) = get_top(arr, f, 0)
        (t_top, t_l) = get_top(arr, t, 0)
        if f_l < t_l:
            arr[f_top] = t_top
        else:
            arr[t_top] = f_top

    for i in range(2 * n):
        arr[i], _ = get_top(arr, i, 0)

    counter = [v for k, v in dict(Counter(arr)).items() if v > 1]
    counter.sort()
    print(counter[0], counter[-1])




