# problem https://www.hackerrank.com/challenges/contacts/problem


def add(trie, word):
    trie[0] += 1
    if len(word) == 0:
        return

    if word[0] not in trie[1].keys():
        trie[1][word[0]] = [0, {}]

    add(trie[1][word[0]], word[1:])
    return


def find(trie, word):
    if len(word) == 0:
        print(trie[0])
        return

    n, tries = trie

    if word[0] not in tries.keys():
        print(0)
        return

    find(tries[word[0]], word[1:])
    return


if __name__ == '__main__':
    n = int(input())
    commands = [tuple(input().strip().split()) for _ in range(n)]

    t = [0, {}]
    for c in commands:
        if c[0] == 'add':
            add(t, c[1])
        else:
            find(t, c[1])


