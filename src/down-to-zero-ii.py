# problem https://www.hackerrank.com/challenges/down-to-zero-ii/problem
from math import ceil, sqrt, floor


def fermat(n):
    """
    Fermat factorisation algorithm
    """
    if n == 2:
        return None
    if n % 2 == 0:
        return int(n / 2), 2
    for x in range(ceil(sqrt(n)), n):
        y_s = x * x - n
        y = sqrt(y_s)
        t = int(x + y)
        s = int(x - y)
        if int(y) ** 2 == y_s and t not in (1, n) and s not in (1, n):
            return max(t, s), min(t, x)
    return None


def process(n):
    dp = [10 ** 7] * (n+1)
    dp[0] = 0
    dp[1] = 1
    for i in range(2, n + 1):
        dp[i] = min(dp[i-1] + 1, dp[i])
        for j in range(2, min(i + 1, ceil(n / (i + 1)))):
            dp[j * i] = min(dp[i] + 1, dp[j * i])

    return dp


if __name__ == '__main__':
    q = int(input())
    ns = [int(input()) for _ in range(q)]
    result = process(10**6 + 1000)
    for r in ns:
        print(result[r])
