# problem https://www.hackerrank.com/challenges/game-of-two-stacks/problem
import copy


def calc(p, q, x):
    aa = p[:]
    bb = q[:]
    score = 0
    aa_tmp = []
    summ = 0
    while aa and summ + aa[-1] <= x:
        score += 1
        v = aa.pop()
        summ += v
        aa_tmp.append(v)

    max_score = score
    while aa_tmp:
        while bb and summ + bb[-1] <= x:
            score += 1
            summ += bb.pop()
        max_score = max(max_score, score)

        score -= 1
        summ -= aa_tmp.pop()

    while bb and summ + bb[-1] <= x:
        score += 1
        summ += bb.pop()
    max_score = max(max_score, score)

    return max_score


if __name__ == '__main__':
    g = int(input())
    for _ in range(g):
        n, m, x = tuple(map(int, input().strip().split()))
        a = list(reversed(list(map(int, input().strip().split()))))
        b = list(reversed(list(map(int, input().strip().split()))))
        print(calc(a, b, x))


"""
1
12 23 87
10 12 15 3 19 12 13 12 15 1 18 18
2 19 16 16 7 12 10 9 2 16 12 1 0 3 3 3 16 8 2 6 12 17 2
"""