# problem https://practice.geeksforgeeks.org/problems/geek-and-his-tricky-series/0

base = 10 ** 9 + 7


def calc_value(n):
    if n == 0:
        return 7
    else:
        return (calc_value(n - 1) * 2 + n) % base


if __name__ == '__main__':
    n = int(input())
    for _ in range(n):
        print(calc_value(int(input()) - 1))
