# problem https://practice.geeksforgeeks.org/problems/generate-grey-code-sequences/1


def grey(n):
    if n == 1:
        return ["0", "1"]
    else:
        prev = grey(n - 1)
        rev = list(reversed(prev))
        return list(map(lambda x: "0" + x, prev)) + list(map(lambda x: "1" + x, rev))


if __name__ == '__main__':
    for _ in range(int(input())):
        print(" ".join(grey(int(input()))))
