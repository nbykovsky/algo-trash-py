"""
problem: https://practice.geeksforgeeks.org/problems/gray-code/0
"""


def msb(n):
    assert n >= 0
    pos = 0
    while (n >> pos) != 0:
        pos += 1
    return 1 << (pos - 1)


def gray_code(n):
    if n == 0:
        return 0
    else:
        m = msb(n)
        k = (m << 1) - 1 - n
        return m | gray_code(k)


def gray_code_better(n):
    """
    Explanation: https://practice.geeksforgeeks.org/editorial.php?pid=3253
    """
    return n ^ (n >> 1)


if __name__ == '__main__':
    for _ in range(int(input())):
        print(gray_code(int(input())))
