# problem https://practice.geeksforgeeks.org/problems/how-many-xs/0
# explanation https://www.quora.com/How-many-times-will-each-digit-from-0-to-9-appear-when-all-numbers-from-1-to-N-are-written-in-decimal-notation/answer/Raziman-T-V?ch=10&share=3c7b2441&srid=uH4DZ
from math import log10, floor


def calc_position(dig, num, pos):
    """
    Calculates how many times 'digit' appears in number less then 'number' on 'position'
    """
    assert 0 < pos <= log10(num) + 1
    assert 0 <= dig <= 9

    # selecting digit on position pos
    target_digit = (num % (10 ** pos)) // (10 ** (pos - 1))
    if target_digit == dig == 0:
        return (num // (10 ** pos) - 1) * (10 ** (pos - 1)) + num % (10 ** (pos - 1)) + 1
    elif dig == 0 < target_digit:
        return (num // (10 ** pos)) * (10 ** (pos - 1))
    elif dig < target_digit:
        return (num // (10 ** pos) + 1) * (10 ** (pos - 1))
    elif dig == target_digit:
        return (num // (10 ** pos)) * (10 ** (pos - 1)) + num % (10 ** (pos - 1)) + 1
    elif dig > target_digit:
        return (num // (10 ** pos)) * (10 ** (pos - 1))
    else:
        raise Exception("Wrong")


def calc(dig, num):
    result = 0
    for pos in range(1, floor(log10(num)) + 2):
        result += calc_position(dig, num, pos)
    return result


def calc_naive(x, l, u):
    num = 0
    for n in range(l+1, u):
        num += str(n).count(str(x))
    return num


if __name__ == '__main__':
    for _ in range(int(input())):
        x = int(input())
        l, u = map(int, input().strip().split())
        print(calc(x, u - 1) - calc(x, l))

