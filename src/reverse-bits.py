"""
Problem: https://practice.geeksforgeeks.org/problems/reverse-bits/0/
"""


def rev(num):
    """
    Complexity  O(#bits)
    """
    acc = 0
    for i in range(0, 16):
        acc |= ((((1 << i) & num) << 31 - i * 2) | (((1 << (31 - i)) & num) >> 31 - i * 2))
    return acc


def rev_better(num):
    """
    More simple solution
    """
    cnt = 32
    acc = 0
    while num != 0:
        acc <<= 1
        acc |= num & 1
        num >>= 1
        cnt -= 1
    acc <<= cnt
    return acc


if __name__ == "__main__":
    for _ in range(int(input())):
        print(rev(int(input())))
