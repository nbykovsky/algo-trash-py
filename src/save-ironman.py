# problem https://practice.geeksforgeeks.org/problems/save-ironman/0
import math


def palindrome(s):
    for i in range(math.floor(len(s) / 2) + 1):
        if s[i] != s[-i - 1]:
            return "NO"

    return "YES"


if __name__ == '__main__':
    t = int(input())
    for _ in range(t):
        print(palindrome(list(filter(lambda x: x.isalnum(), input().lower()))))
