# problem https://www.hackerrank.com/challenges/simple-text-editor/problem
import copy
import sys

ss = ""
undo = []


def process(request_type, payload, undoing=False):
    global ss, undo
    if request_type == '1':
        if not undoing:
            undo.append(('2', len(payload)))
        ss += payload
    elif request_type == '2':
        if not undoing:
            undo.append(('1', ss[-int(payload):]))
        ss = ss[:-int(payload)]
    elif request_type == '3':
        print(ss[int(payload)-1])
    elif request_type == '4':
        command = copy.deepcopy(undo[-1])
        del undo[-1]
        process(*command, undoing=True)


if __name__ == '__main__':
    q = int(input())
    for x in range(q):
        line = sys.stdin.readline().replace('\n', '')
        cmd = line.split(" ")
        if len(cmd) == 1:
            cmd.append(None)
        process(*cmd)

