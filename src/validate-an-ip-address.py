# problem https://practice.geeksforgeeks.org/problems/validate-an-ip-address/1
# theory https://docs.python.org/3/howto/regex.html#regex-howto

import re

if __name__ == '__main__':
    t = int(input())
    p = re.compile(
         r"^(\d|[1-9]\d|1\d\d|2[0-5][0-5])\."
          r"(\d|[1-9]\d|1\d\d|2[0-5][0-5])\."
          r"(\d|[1-9]\d|1\d\d|2[0-5][0-5])\."
          r"(\d|[1-9]\d|1\d\d|2[0-5][0-5])$")
    for _ in range(t):
        if p.match(input()):
            print(1)
        else:
            print(0)
