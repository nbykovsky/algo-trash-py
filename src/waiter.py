# problem https://www.hackerrank.com/challenges/waiter/problem


def era(upper_bound):
    prime = []
    ind = [True] * (upper_bound + 2)
    ind[0] = ind[1] = False
    for i in range(2, upper_bound):
        if ind[i]:
            prime.append(i)
            for j in range(i * 2, upper_bound, i):
                ind[j] = False
    return prime


if __name__ == '__main__':
    n, q = map(int, input().rstrip().split())
    a = map(int, input().rstrip().split())
    result = [[] for _ in range(q)]
    offset = []
    primes = era(10**4)
    for e in a:
        broken = False
        for idx in range(q):
            if e % primes[idx] == 0:
                result[idx].append(e)
                broken = True
                break
        if not broken:
            offset.append(e)

    for idx, r in enumerate(result):
        if idx % 2 == 1:
            result[idx].reverse()

    if q % 2 == 0:
        offset.reverse()

    result.append(offset)

    for y in sum(result, []):
        print(y)



